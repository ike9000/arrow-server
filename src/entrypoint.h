#ifdef _MSC_VER
#	pragma warning ( disable: 4786 )
#endif //_MSC_VER
#include <string>
#include <cstring>
//#include <stdio.h>
#include "NprocServer.h"
#include "hef_http_header_collector.h"
namespace hef {}
using namespace nproc;
using namespace hef;

class HssdCli : public NprocISrvOnClientNotify
{
public:
	;            HssdCli( int argc, const char*const* argv );
	virtual      ~HssdCli();
	int          run2();
	void         setTheRunFlag( bool inp ) { bRun2 = inp; }
	bool         isServerValid( std::string* err = 0 )const;
	virtual bool nprocOnClientConnect( const NprocSClientConnecting& in );
	virtual bool nprocOnDataReadFromClient( const NprocSDataReadFromClient& in );
	virtual bool nprocOnClientIdleOrNoMoreData( const NprocSClientIdleNoMoreData& in );
	virtual void nprocOnConnectionClosed( size_t id_ );
	//virtual void NprocSrvPrintf( const char* inp, size_t uVerbLevel );
private:
	typedef std::pair<int,std::string> TyMsgPair;
	typedef std::vector<TyMsgPair>     TyMsgFlags; //using TyMsgFlags = std::vector<TyMsgPair>;
	typedef std::vector<std::pair<std::string,std::string> > TyStrStrVec;
	struct SRqCln;
	static void defaultStrToInt( const char* a, int* d );
	std::vector<SRqCln>::iterator findClinetById( size_t ident2 );
	SRqCln* findClinetById2( size_t ident2 );
	void autoPurgeClientsEtc( NprocServer& nsrv2 );
	void fireShellCommandsIfAny();
	bool detectHammeringClientCmd( size_t idClient, uint32_t tmNow3 );
private:
	/// Flags from argv for the program, used with 'lsMsgFlags'.
	enum{
		EMF_ShowCmdOpts = 0x1,      //--bShowCmdOpts
		EMF_ShowHttpRqText = 0x2,   //--bShowHttpRqText
		EMF_ShowIdleSw = 0x4,       //--bShowIdleSw
		EMF_ShowCaption = 0x8,      //--bShowCaption
		EMF_UseSigInt = 0x10,       //--bUseSigInt
		EMF_ShowConnects = 0x20,    //--bShowConnecting
		EMF_SendVerAsStatus = 0x40, //--bSendVerAsStatus
		EMF_ShowWarnings = 0x80,    //--bShowWarnings
	};
	struct SRqCln{ //single request-plus-client.
		size_t                  ident;
		std::string             ipaddr, hostnm;
		std::vector<uint8_t>    data2;
		HfHTTPHeaderCollector   cltr;
		uint32_t                uTotalBytes, tmConnectedAt, tmRecentRq;
		bool                    bGotAllHttp;
		SRqCln() : cltr(HF_EHHHCF_CollectClientRqInstead), uTotalBytes(0), tmConnectedAt(0), tmRecentRq(0), bGotAllHttp(0) {}
	};
	struct SNewCmd{
		SRqCln      cln2;
		std::string rqQryPathEtc; // fe. "/?a=1", "/index.php?a=1&b=1".
		uint32_t    tmCmdAddedAt;
	};
	struct STtxItm{ // throttle item.
		std::string ipaddr2;
		size_t      idConn2;
		uint32_t    tmConnAddedAt;
	};
	const char*              szAppName = "Arrow Server", *szAppAbbr = "ARSRV";
	const char*              szVersion = "1.1";
	const std::string        VerStr = std::string(szVersion) +" [" +std::string(__DATE__) +"]";
	bool                     bRun2 = 1;
	int                      nDbgMsgFlags; // fe. EMF_ShowIdleSw
	int                      nPurged = 0, nThrTotal = 0;
	uint32_t                 uMaxRqSizeB = 65536, nPortNr = 7007, uPurgeTimeout = 16000; // --
	std::vector<int>         aSleepPerformance = {50,2000,500,}; // -- <MS,MS,MS>
	std::vector<std::string> aWhiteListCIPs; // --
	uint32_t                 uHammerInterval = 500; // --
	std::string              Cmd, Err;
	const TyMsgFlags         lsMsgFlags;
	std::vector<SRqCln>      Rqss;
	std::vector<SNewCmd>     NewCmds;
	int                      nMaxConnPerServer = 8, nMaxConnPerClient = 4; // --
	std::vector<STtxItm>     Ttx;
	std::pair<int,int>       nMaxConnPerMsPerServer = {32,1000,}; // --
	std::pair<int,int>       nMaxConnPerMsPerClient = {16,1000,}; // --
};

//reinterpret_cast<>
//static_cast<>
