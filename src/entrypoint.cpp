#include "entrypoint.h"
#include <algorithm>
//#include <stdio.h>
//#include <cctype>
//#include <stdlib.h> //exit()
//#include <cstring>
//#include <string.h>
#ifdef WIN32
#	include <ctime>  //Win: time()
#endif
#include <signal.h> //signal()
#include <assert.h>
#include "hef_data.h"
#include "hef_str.h"
#include "hef_uri_syntax.h"
#include "hef_str_args.h"

static HssdCli* ptrHssdCli = 0;

/// Constructor.
HssdCli::HssdCli( int argc, const char*const* argv )
	: nDbgMsgFlags(
			EMF_ShowIdleSw|EMF_ShowCaption|EMF_UseSigInt|
			EMF_SendVerAsStatus|EMF_ShowWarnings )
	, lsMsgFlags({
			{EMF_ShowIdleSw,     "--bShowIdleSw"},
			{EMF_ShowHttpRqText, "--bShowHttpRqText"},
			{EMF_ShowCmdOpts,    "--bShowCmdOpts"},
			{EMF_ShowCaption,    "--bShowCaption"},
			{EMF_UseSigInt,      "--bUseSigInt",},
			{EMF_ShowConnects,   "--bShowConnecting"},
			{EMF_SendVerAsStatus,"--bSendVerAsStatus"},
			{EMF_ShowWarnings,   "--bShowWarnings"},
	})
{
	//size_t numva = print2c_va( "abc", -1, -2, -3 )();
	//printf("numva:%d\n", (int)numva );
	{
		const char* sz2, *sz3;
		typename TyMsgFlags::const_iterator a;
		for( int i=0; i<argc; i++ ){
			sz2 = argv[i];
			sz3 = ( i+1 < argc ? argv[i+1] : "" );
			if(0){
			}else if( !strcmp(sz2,"--uMaxRqSizeB") ){
				uMaxRqSizeB = atoi(sz3);
				i++;
			}else if( !strcmp(sz2,"--nPortNr") ){
				nPortNr = atoi(sz3);
				i++;
			}else if( !strcmp(sz2,"--aSleepPerformance") ){
				std::vector<std::string> lsx;
				hf_explode( sz3, ",", lsx, ",\x20" );
				i++;
				lsx.resize(3,"");
				assert( aSleepPerformance.size() >= 3 );
				defaultStrToInt( lsx[0].c_str(), &aSleepPerformance[0] );
				defaultStrToInt( lsx[1].c_str(), &aSleepPerformance[1] );
				defaultStrToInt( lsx[2].c_str(), &aSleepPerformance[2] );

			}else if( !strcmp(sz2,"--nMaxConnPerMsPerServer") ){
				std::vector<std::string> lsx;
				hf_explode( sz3, "/", lsx, "\x20", 1 );
				i++;
				lsx.resize(2,"");
				defaultStrToInt( lsx[0].c_str(), &nMaxConnPerMsPerServer.first );
				defaultStrToInt( lsx[1].c_str(), &nMaxConnPerMsPerServer.second );
			}else if( !strcmp(sz2,"--nMaxConnPerMsPerClient") ){
				std::vector<std::string> lsx;
				hf_explode( sz3, "/", lsx, "\x20", 1 );
				i++;
				lsx.resize(2,"");
				defaultStrToInt( lsx[0].c_str(), &nMaxConnPerMsPerClient.first );
				defaultStrToInt( lsx[1].c_str(), &nMaxConnPerMsPerClient.second );
			}else if( !strcmp(sz2,"--aWhiteListCIPs") ){
				hf_explode( sz3, ",", aWhiteListCIPs, ",\x20" );
				i++;
			}else if( !strcmp(sz2,"--uPurgeTimeout") ){
				uPurgeTimeout = atoi(sz3);
				i++;
			}else if( !strcmp(sz2,"--uHammerInterval") ){
				uHammerInterval = atoi(sz3);
				i++;
			}else if( !strcmp(sz2,"--Cmd") ){
				Cmd = sz3;
				i++;
			}else if( !strcmp(sz2,"--nMaxConnPerServer") ){
				nMaxConnPerServer = atoi(sz3);
				i++;
			}else if( !strcmp(sz2,"--nMaxConnPerClient") ){
				nMaxConnPerClient = atoi(sz3);
				i++;
			}else{
				for( a = lsMsgFlags.begin(); a != lsMsgFlags.end(); ++a ){
					if( !strcmp( sz2, a->second.c_str() ) ){
						int v = atoi(sz3);
						nDbgMsgFlags = ( v?
							nDbgMsgFlags | a->first :
							nDbgMsgFlags & (~a->first)
						);
						i++;
						break;
					}
				}
			}
		}
	}
	if( nDbgMsgFlags & EMF_ShowCaption ){
		// fe: "Xxxxxx Xxxxxx - version 0.1 [Sep 30 2016]" //(ARSRV)
		printf("\n""%s - ARSRV Version %s\n", szAppName, VerStr.c_str() );
		printf("-------------------------------------------------------\n");
	}
	if( nDbgMsgFlags & EMF_ShowCmdOpts ){
		assert( !lsMsgFlags.empty() );
		const TyMsgPair* ptrMaxEl = &*std::max_element( lsMsgFlags.begin(), lsMsgFlags.end(),
			[]( const TyMsgPair& a, const TyMsgPair& b ){
				return static_cast<bool>( a.second.size() < b.second.size() );
		});
		typename TyMsgFlags::const_iterator a; char fmt2[128], fmt3[128];
		int nMaxSpc = static_cast<int>( ptrMaxEl->second.size() );
		nMaxSpc = std::max<int>( nMaxSpc, strlen("--aSleepPerformance") );
		nMaxSpc = std::max<int>( nMaxSpc, strlen("--nMaxConnPerClient") );
		nMaxSpc = std::max<int>( nMaxSpc, strlen("--nMaxConnPerMsPerServer") );
		nMaxSpc = std::max<int>( nMaxSpc, strlen("--nMaxConnPerMsPerClient") );
		sprintf(fmt2,"%%-%d""s: %%s\n", static_cast<int>(nMaxSpc) );
		printf( fmt2, "--Cmd", Cmd.c_str() );
		sprintf(fmt2,"%%-%d""s: %%s\n", nMaxSpc );
		for( a = lsMsgFlags.begin(); a != lsMsgFlags.end(); ++a ){
			// fmt2 is fe: "%-20s: %s\n"
			printf( fmt2, a->second.c_str(), ( a->first & nDbgMsgFlags ? "On" : "Off" ) );
		}
		assert( aSleepPerformance.size() >= 3 );
		sprintf(fmt3,"%%-%d""s: %%d,%%d,%%d (ms)\n", nMaxSpc );
		printf( fmt3, "--aSleepPerformance", aSleepPerformance[0], aSleepPerformance[1], aSleepPerformance[2] );
		//
		sprintf(fmt2,"%%-%d""s: %%d %%s\n", nMaxSpc );
		printf( fmt2, "--uMaxRqSizeB", static_cast<int>(uMaxRqSizeB), "(bytes)" );
		printf( fmt2, "--uPurgeTimeout", static_cast<int>(uPurgeTimeout), "(ms)" );
		printf( fmt2, "--nPortNr", static_cast<int>(nPortNr), "" );
		printf( fmt2, "--uHammerInterval", static_cast<int>(uHammerInterval), "(ms)" );
		printf( fmt2, "--nMaxConnPerServer", nMaxConnPerServer, "" );
		printf( fmt2, "--nMaxConnPerClient", nMaxConnPerClient, "" );
		sprintf(fmt2,"%%-%d""s: %%d/%%d %%s\n", nMaxSpc );
		printf( fmt2, "--nMaxConnPerMsPerServer", nMaxConnPerMsPerServer.first, nMaxConnPerMsPerServer.second, "(num/ms)" );
		printf( fmt2, "--nMaxConnPerMsPerClient", nMaxConnPerMsPerClient.first, nMaxConnPerMsPerClient.second, "(num/ms)" );
		{
			sprintf(fmt2,"%%-%d""s: %%s", nMaxSpc );
			printf( fmt2, "--aWhiteListCIPs", "");
			int i = 0;
			for( const auto& a : aWhiteListCIPs ){
				if( i >= 3 ){
					printf("...");
					break;
				}else{
					printf("%s, ", a.c_str() );
				}
				i++;
			}
			printf("\n");
		}
		printf("\n");
	}
	if( Cmd.empty() && nDbgMsgFlags & EMF_ShowWarnings ){
		//isServerValid()
		printf("ARSRV: WARNING: Shell command is empty (see '--Cmd' argument).\n");
	}
}
HssdCli::~HssdCli()
{
}
/**
	Use this Wget command to test HTTP connection:
		$> wget http://127.0.0.1:8080/ -O-
*/
int main( int argc, const char*const* argv )
{
	std::string msg;
	HssdCli hssd( argc, argv );
	if( !hssd.isServerValid( &msg ) ){
		printf("ERROR: %s\n", ( !msg.empty() ? msg.c_str() : "unknown failure." ) );
		return 1;
	}
	ptrHssdCli = &hssd;
	int res = hssd.run2();
	printf("ARSRV: Program exit [%d]...\n", res );
	ptrHssdCli = 0;
	return res;
}
bool HssdCli::isServerValid( std::string* err )const
{
	//std::string err3, *err2 = ( err ? err : &err3 );
	//if( Cmd.empty() ){
	//	*err2 = "Shell command is empty (please see '--Cmd' argument).";
	//	return 0;
	//}
	return 1;
}
class CPrntfHndlr : public NprocISrvPrintfHandler {
public:
	virtual void NprocSrvPrintf( const char* inp, size_t uVerbLevel )
	{
	//	printf("ARSRV: NprocSrvPrintf: [%s]\n", inp );
	}
};

void HssdCli::defaultStrToInt( const char* a, int* d )
{
	if( strlen(a) )
		*d = atoi(a);
	//else *d = *d;
}
// Define the function to be called when ctrl-c (SIGINT) signal is sent to process
// ref: http://www.yolinux.com/TUTORIALS/C++Signals.html
void signal_callback_handler( int signum )
{
	printf("\n""ARSRV: Caught signal: [%d]; triggering cleanup and terminate...\n", signum );
	if( ptrHssdCli ){
		ptrHssdCli->setTheRunFlag( 0 );
	}
	// Cleanup and close up stuff here. Terminate program...
	//exit( signum ); //no exit() call here, allow gracefull auto-exit.
}
/*
class print2c_va {
public:
	/// Ctor.
	template<typename T, typename... Types>
	print2c_va( T firstArg, Types... args ){
		print_l( firstArg, args... );
		nNum = sizeof...(args) + 1;
	}
	void print_l() {}
	template<typename T, typename... Types>
	void print_l( T firstArg, Types... args ){
		print_single( firstArg );
		//if constexpr( sizeof...(args) > 0 ){
		//	print_l( args... );
		//}
		print_l( args... );
	}
	size_t operator()()const {return n();}
	size_t operator*()const {return n();}
	size_t n()const {return nNum;}
	void print_single( int a ){
		printf("%d\n", a );
	}
	void print_single( const char* sz ){
		printf("%s\n", sz );
	}
	size_t nNum = 0;
};//*/
int HssdCli::run2()
{
	printf("ARSRV: Starting HTTP server, port %d, use Ctrl-C to terminate...\n", static_cast<int>(nPortNr) );
	CPrntfHndlr cPrntfHndlr;
	if( nDbgMsgFlags & EMF_UseSigInt )
		signal( SIGINT, signal_callback_handler );	// Register signal and signal handler
	NprocSServer sns;
	sns.uServerPort = nPortNr;
	sns.notify2     = this;
	sns.Prntfh      = &cPrntfHndlr;
	sns.flags2      = NPROC_ENPPF_WsaStartup;
	NprocServer nsrv( sns );
	if( !nsrv.isValid() ){
		printf("ARSRV: ERROR: Server create failed [%s].\n", nsrv.getMsgOnConstruct() );
		return 1;
	}else{
		uint32_t tmNxt, tmPrev = hf_getGlobalTicks(), tmIdleDelta = 0, tmDelta, tmSlp = 0;
		for( ; bRun2; ){
			bool bWasIdleOut = 0;
			nsrv.perform2( &bWasIdleOut );
			tmNxt = hf_getGlobalTicks();
			tmDelta = tmNxt - tmPrev;
			tmIdleDelta = ( bWasIdleOut ? tmIdleDelta + tmDelta : 0 );
			uint32_t tmOldSlp = tmSlp;
			tmSlp = ( tmIdleDelta > static_cast<uint32_t>(aSleepPerformance[1]) ? aSleepPerformance[2] : aSleepPerformance[0] );
			if( nDbgMsgFlags & EMF_ShowIdleSw && tmOldSlp != tmSlp ){
				if( tmSlp == static_cast<uint32_t>(aSleepPerformance[2]) ){
					printf("ARSRV: Switching to idle mode.\n");
					//else - printf("Switching to active mode.\n");
				}
			}
			nproc_Sleep( tmSlp );
			autoPurgeClientsEtc( nsrv );

			if( !Cmd.empty() )
				fireShellCommandsIfAny();
			tmPrev = tmNxt;
		}
	}
	return 0;
}
std::vector<HssdCli::SRqCln>::iterator HssdCli::findClinetById( size_t ident2 )
{
	std::vector<SRqCln>::iterator a;
	a = std::find_if( Rqss.begin(), Rqss.end(), [&]( const SRqCln& a ){
			return a.ident == ident2;
	});
	return a;
}
HssdCli::SRqCln* HssdCli::findClinetById2( size_t ident2 )
{
	std::vector<SRqCln>::iterator a = findClinetById( ident2 );
	if( a != Rqss.end() )
		return &*a;
	return 0;
}
void HssdCli::autoPurgeClientsEtc( NprocServer& nsrv2 )
{
	int64_t tmNow = hf_getGlobalTicks();
	{
		std::vector<SRqCln>::iterator a;
		for( a = Rqss.begin(); a != Rqss.end(); ){
			if( tmNow >= a->tmConnectedAt + uPurgeTimeout ){
				printf("ARSRV: Auto purge inactive client [%s]\n", a->ipaddr.c_str() );
				nPurged++;
				if( !nsrv2.closeConnection( a->ident, NPROC_ECCF_NoNotifyCallback ) ){
					printf("ARSRV: Error, connection close failed. [%s]\n", a->ipaddr.c_str() );
				}
				a = Rqss.erase(a);
			}else{
				++a;
			}
		}
	}{
		std::vector<STtxItm>::iterator a; bool r;
		for( a = Ttx.begin(); a != Ttx.end(); ){
			r = a->tmConnAddedAt < tmNow - nMaxConnPerMsPerServer.second &&
				a->tmConnAddedAt < tmNow - nMaxConnPerMsPerClient.second;
			if( r ){
				a = Ttx.erase(a);
			}else
				++a;
		}
	}
}
bool HssdCli::nprocOnClientConnect( const NprocSClientConnecting& inp )
{
	bool bAccept = aWhiteListCIPs.empty();
	if( !aWhiteListCIPs.empty() ){
		std::vector<std::string>::const_iterator a;
		a = std::find_if( aWhiteListCIPs.begin(), aWhiteListCIPs.end(),
			[&]( const std::string& a ){
				return a == inp.strIPAddr;
		});
		if( a != aWhiteListCIPs.end() ){
			bAccept = 1;
		}else{
			bAccept = 0;
			if( nDbgMsgFlags & EMF_ShowConnects )
				printf("ARSRV: Rejected client, not on the wite-list [%s]\n", inp.strIPAddr.c_str() );
		}
	}
	if( bAccept ){ // throttle global connects.
		int n = static_cast<int>( Rqss.size() );
		if( n+1 > nMaxConnPerServer ){
			bAccept = 0;
			nThrTotal++;
			if( nDbgMsgFlags & EMF_ShowConnects )
				printf("ARSRV: Rejected client, server global connect limit reached [%s]\n", inp.strIPAddr.c_str() );
		}
	}
	int64_t tmNow4 = hf_getGlobalTicks();
	if( bAccept ){
		int n; // throttle max connections per server.
		n = std::count_if( Ttx.begin(), Ttx.end(), [&]( const STtxItm& a ){
			return a.tmConnAddedAt >= tmNow4 - nMaxConnPerMsPerServer.second;
		});
		if( n+1 > nMaxConnPerMsPerServer.first ){
			bAccept = 0;
			nThrTotal++;
			if( nDbgMsgFlags & EMF_ShowConnects ){
				printf("ARSRV: Rejected client, %d/%d""ms limit reached (s) [%s]\n",
					nMaxConnPerMsPerServer.first, nMaxConnPerMsPerServer.second, inp.strIPAddr.c_str() );
			}
		}
	}
	if( bAccept ){
		int n; // throttle max connections per ms per client.
		n = std::count_if( Ttx.begin(), Ttx.end(), [&]( const STtxItm& a ){
			if( a.ipaddr2 == inp.strIPAddr.c_str() ){
				if( a.tmConnAddedAt >= tmNow4 - nMaxConnPerMsPerClient.second )
					return 1;
			}
			return 0;
		});
		if( n+1 > nMaxConnPerMsPerClient.first ){
			bAccept = 0;
			if( nDbgMsgFlags & EMF_ShowConnects ){
				printf("ARSRV: Rejected client, %d/%d""ms limit reached (c) [%s]\n",
					nMaxConnPerMsPerClient.first, nMaxConnPerMsPerClient.second, inp.strIPAddr.c_str() );
			}
		}
	}
	if( bAccept ){
		int n; // throttle per-client connects.
		n = std::count_if( Rqss.begin(), Rqss.end(), [&]( const SRqCln& a ){
			return a.ipaddr == inp.strIPAddr.c_str();
		});
		if( n+1 > nMaxConnPerClient ){ // if client is connecting too many times.
			bAccept = 0;
			if( nDbgMsgFlags & EMF_ShowConnects )
				printf("ARSRV: Rejected client, too many connects [%s]\n", inp.strIPAddr.c_str() );
		}
	}
	if( bAccept ){
		if( nDbgMsgFlags & EMF_ShowConnects )
			printf("ARSRV: Accepted client [%s]\n", inp.strIPAddr.c_str() );
		STtxItm tt; // setup throttle item.
		tt.ipaddr2       = inp.strIPAddr.c_str();
		tt.idConn2       = inp.id2;
		tt.tmConnAddedAt = hf_getGlobalTicks();
		Ttx.push_back(tt);
		SRqCln cln;
		cln.ident         = inp.id2;
		cln.ipaddr        = inp.strIPAddr.c_str();
		cln.hostnm        = inp.strHostname.c_str();
		cln.tmConnectedAt = hf_getGlobalTicks();
		Rqss.push_back( cln );
	}
	return bAccept;
}
bool HssdCli::
nprocOnClientIdleOrNoMoreData( const NprocSClientIdleNoMoreData& inp )
{
	std::string msgb;
	std::vector<SRqCln>::iterator a = findClinetById( inp.id2 );
	assert( a != Rqss.end() && !!"Unknown client on no-more-data." );
	if( a->bGotAllHttp ){
		msgb += "OK";
	}else{
		msgb += "??";
	}
	if( EMF_SendVerAsStatus & nDbgMsgFlags ){
		msgb += ("; Version internal: " + std::string(szVersion) );
	}
	char bfr[64];
	sprintf(bfr,"; %X", static_cast<unsigned int>(time(0)) );
	msgb += bfr;
	std::vector<uint8_t> data2 = nproc_CreateSimpleHTTPOKResponse( msgb.c_str() );
	inp.sendBuffer->insert( inp.sendBuffer->end(), data2.begin(), data2.end() );
	//autoPurgeClientsEtc()
	//for( int i=0; i<32 && i< static_cast<int>data2.size(); i++ ){
	//	inp.sendBuffer->push_back( data2[i] );
	//}
	return 0;
}
bool HssdCli::
nprocOnDataReadFromClient( const NprocSDataReadFromClient& inp )
{
	std::vector<SRqCln>::iterator a = findClinetById( inp.id2 );
	assert( a != Rqss.end() && "Unknown client on more-data." );
	a->uTotalBytes += inp.size;
	if( a->uTotalBytes > uMaxRqSizeB ){
		printf("ARSRV: Discarding this client, sends too much data.\n");
		printf("       [%s / %s]\n", a->ipaddr.c_str(), a->hostnm.c_str() );
		printf("       total: %d (uMaxRqSizeB %d)\n", static_cast<int>(a->uTotalBytes), static_cast<int>(uMaxRqSizeB) );
		return 0;
	}
	if( a->bGotAllHttp ){
		return 0;
	}
	const uint8_t* pos = inp.data;
	const uint8_t* end = inp.data + inp.size;
	size_t i;
	if( nDbgMsgFlags & EMF_ShowHttpRqText ){
		for(; pos<end; ){
			printf("ARSRV: " );
			for( i=0; i<64 && pos<end; i++, pos++ ){
				printf("%c", static_cast<char>( isprint(*pos) ? *pos : (*pos=='\r'||*pos=='\n' ? '^':'.') ) );
			}
			printf("\n" );
		}
	}
	size_t dmy0 = 0, dmy1 = 0;
	bool bGotAll = 0, rs2 = 0;
	std::string err;
	rs2 = a->cltr.moreData( inp.data, inp.size, &dmy0, err, &bGotAll, &dmy1 );
	if( !rs2 ){
		printf("ARSRV: Error, client bad data. [%s] [%s]\n", err.c_str(), a->ipaddr.c_str() );
		return 0;
	}
	if( bGotAll ){
		uint32_t tmNow2 = hf_getGlobalTicks();
		a->tmRecentRq = tmNow2;
		if( detectHammeringClientCmd( a->ident, tmNow2 ) ){
			std::string str = a->cltr.getRequestText();
			printf("ARSRV: Client hammering detected. Ignoring request.\n");
			printf("       [%s] [%s]\n", a->ipaddr.c_str(), str.substr(0,42).c_str() );
		}else{
			printf("ARSRV: Got all data [%s]\n", a->ipaddr.c_str() );
			printf("       request: [%s]\n", std::string(a->cltr.getRequestText()).substr(0,42).c_str() );
			SNewCmd ncm;
			ncm.cln2           = *a;
			ncm.rqQryPathEtc   = a->cltr.getRequestText(); //fe. "/file.txt?a=1"
			ncm.tmCmdAddedAt   = tmNow2;
			NewCmds.push_back( ncm );
		}
		a->bGotAllHttp = 1;
	}
	return 1;
}
void HssdCli::nprocOnConnectionClosed( size_t ident2 )
{
	std::vector<SRqCln>::iterator a = findClinetById( ident2 );
	if( nDbgMsgFlags & EMF_ShowConnects ){
		printf("ARSRV: Client disconnect [%s]\n",
				( a != Rqss.end() ? a->ipaddr.c_str() : "?.?.?.?" ) );
	}
	if( a != Rqss.end() ){
		Rqss.erase(a);
	}
}
bool HssdCli::detectHammeringClientCmd( size_t idClient, uint32_t tmNow3 )
{
	for( const auto a : NewCmds ){ //std::vector<SNewCmd> NewCmds;
		if( a.cln2.ident == idClient ){
			if( a.tmCmdAddedAt <= tmNow3 ){
				uint32_t dt = tmNow3 - a.tmCmdAddedAt;
				if( dt <= uHammerInterval ){
					return 1;
				}
			}
		}
	}
	return 0;
}
void HssdCli::fireShellCommandsIfAny()
{
	for( const auto& a : NewCmds ){
		// a.rqQryPathEtc is fe. "/?a=2", "/file.txt?a=2&b=2".
		HfURISyntax uri2("http","dummy", a.rqQryPathEtc.c_str() );
		std::vector<std::string> lsx2, lsx4;
		// split query string into key-val list, using potentially
		// percent-escaped query string (ie. getRawQuery not getQuery()).
		hf_explode( uri2.getRawQuery().c_str(), "&", lsx2, "", -1 );
		TyStrStrVec lsRq; //{rq.??}
		for( const auto& b : lsx2 ){ // for each URI amp-separated k=v string pairs.
			hf_explode( b.c_str(), "=", lsx4, "", 1 );
			lsx4.resize(2);
			for( auto& c : lsx4 ){ // for both, key and val, percent re-encode.
				std::string str, str3;
				// decode then encode back, do not rely on percent-encoding
				// done on the client side (ie. dealing with external data).
				// encode below must escape all shell specific characters, fe. tilde.
				str = HfURISyntax::decodeStr( c.c_str() );
				c = HfURISyntax::encodeStr( str.c_str(), "/~\x24\x5C:;*?" ); // 0x24=dollar, 0x5C=backslash.
			}
			std::string key2 = lsx4[0].c_str();
			std::string val2 = lsx4[1].c_str(), val3;
			lsRq.push_back( std::make_pair( key2, val2 ) );
		}
		std::string cmd2 = Cmd;
		// text tags "{rq.??}"
		cmd2 = hf_StrReplaceOpCloseTokensCalb( cmd2.c_str(), "{rq.", "}",
				[&]( const char* k, bool& orig )->std::string{
					std::string outp;
					std::find_if( lsRq.begin(), lsRq.end(),
						[&]( const TyStrStrVec::value_type& a )->bool{
								if( a.first != k ) return 0;
								outp = a.second;
								return 1;
						});
					return outp;
				});
		// text tags "{c.??}"
		cmd2 = hf_StrReplaceOpCloseTokensCalb( cmd2.c_str(), "{c.", "}",
				[&]( const char* k, bool& bKeepOrig )->std::string{
						if( !strcmp("ipaddr",k) ){
							return a.cln2.ipaddr;
						}else if( !strcmp("hostname",k) ){
							return a.cln2.hostnm;
						}else if( !strcmp("idConn",k) ){
							return HfArgs("%1").arg2(a.cln2.ident).c_str();
						}else if( !strcmp("uTotalBytes",k) ){
							return HfArgs("%1").arg2(a.cln2.uTotalBytes).c_str();
						}else if( !strcmp("rq",k) ){
							return HfURISyntax::encodeStr( HfURISyntax::decodeStr(a.rqQryPathEtc.c_str()).c_str(), "/~\x24\x5C:;*?" );
						}
						return "";
				});
		// text tags "{s.??}"
		cmd2 = hf_StrReplaceOpCloseTokensCalb( cmd2.c_str(), "{s.", "}",
				[&]( const char* k, bool& bKeepOrig )->std::string{
						if( !strcmp("nPortNr",k) ){
							return HfArgs("%1").arg2( nPortNr ).c_str();
						}else if( !strcmp("version",k) ){
							return szVersion;
						}else if( !strcmp("uMaxRqSizeB",k) ){
							return HfArgs("%1").arg2( uMaxRqSizeB ).c_str();
						}else if( !strcmp("nPurged",k) ){
							return HfArgs("%1").arg( nPurged ).c_str();
						}else if( !strcmp("nThrNow",k) ){
							return HfArgs("%1").arg2( Ttx.size() ).c_str();
						}else if( !strcmp("nThrTotal",k) ){
							return HfArgs("%1").arg2( nThrTotal ).c_str();
						}
						return "";
				} );
		if( !cmd2.empty() ){
			printf("ARSRV: Executing shell command for [%s]\n", a.cln2.ipaddr.c_str() );
			std::vector<std::string> lsx;
			hf_split( cmd2.c_str(), 52, lsx );
			for( const auto& d : lsx ){
				// xxxx"ARSRV: "xxxxxxx
				printf("       %s\n", d.c_str() );
			}
			if( system( cmd2.c_str() ) ){
			}
		}
	}
	NewCmds.clear();
}






